package main

import (
	"crypto/tls"
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/work-projects7/go-netbox-supportcontractfetch/config"
	"gitlab.com/work-projects7/go-netbox-updater/netbox"
	"gitlab.com/work-projects7/go-netbox/client"
	"gopkg.in/yaml.v2"
)

var configData config.ConfigData

func main() {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	configPath := os.Args[1:]
	loadYAML(&configData, configPath[0])
	netboxClient := client.NewNetboxClient(configData.ApiToken, configData.ApiAddress)
	DeviceMap, err := netbox.MakeNetboxDeviceMap(netboxClient)
	if err != nil {
		fmt.Printf("ERROR fetching devices: %v\n", err)
		log.Fatalf("ERROR fetching devices: %v\n", err)
	}
	ParsedDeviceMap := deviceParser(DeviceMap)
	CSVGenerator(ParsedDeviceMap)
}

func deviceParser(DeviceMap map[string]client.Device) map[string]client.Device {
	startDate, err := time.Parse("2006-01-02", configData.StartDate)
	if err != nil {
		fmt.Printf("ERROR converting start date string to a date: %v\n", err)
		log.Fatalf("ERROR converting start date string to a date: %v\n", err)
	}
	endDate, err := time.Parse("2006-01-02", configData.EndDate)
	if err != nil {
		fmt.Printf("ERROR converting end date string to a date: %v\n", err)
		log.Fatalf("ERROR converting end date string to a date: %v\n", err)
	}
	for deviceName, device := range DeviceMap {
		dDate := (*device.CustomFields)["support_contract_expiration"]
		if dDate == nil {
			delete(DeviceMap, deviceName)
			continue
		}
		deviceDate, err := time.Parse("2006-01-02", dDate.(string))
		if err != nil {
			fmt.Printf("ERROR converting device support contract expiration to a date: %v\n", err)
			log.Fatalf("ERROR converting device support contract expiration to a date: %v\n", err)
		}
		if deviceDate.Before(startDate) || deviceDate.After(endDate) {
			delete(DeviceMap, deviceName)
			continue
		}
		if configData.SupportProvider != "" && (*device.CustomFields)["support_provider"].(string) != configData.SupportProvider {
			delete(DeviceMap, deviceName)
			continue
		}
	}
	return DeviceMap
}

func CSVGenerator(DeviceMap map[string]client.Device) {
	csvFile, err := os.OpenFile("results.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	csvwriter := csv.NewWriter(csvFile)

	for deviceName, device := range DeviceMap {
		data := []string{deviceName, device.Serial, device.DeviceType.Model, (*device.CustomFields)["support_provider"].(string), (*device.CustomFields)["support_contract_expiration"].(string)}
		err = csvwriter.Write(data)
		if err != nil {
			fmt.Printf("ERROR failed to write data to .csv: %v\n", err)
			log.Fatalf("ERROR failed to write data to .csv: %v\n", err)
		}
	}
	csvwriter.Flush()
	csvFile.Close()
}

func loadYAML(configData *config.ConfigData, configPath string) {
	currentTime := time.Now() // gets current date and time, for accurate log

	yamlFile, err := os.ReadFile(configPath) // ReadFile has two outputs, the contents of the config file and an err. If working err == nil
	if err != nil {                          // error handling, if YAML fails to read
		fmt.Printf("yamlFile.Get err   #%v ", err)
		log.Fatalf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &configData) // decodes the YAML file and pushes the info to the configData pointer
	if err != nil {                             // error checking if YAML file failed to be decoded
		fmt.Printf("Unmarshal: %v", err)
		log.Fatalf("Unmarshal: %v", err)
	}
	file, err := os.OpenFile(configData.LogPath+currentTime.Format("2006-01-02")+".log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666) // appends/creates/writes log file named after the currentTime, I don't understand the 0666
	if err != nil {                                                                                                                 // error checking if log file fails
		fmt.Printf("Error: %v\n", err)
		log.Fatal(err)
	}
	log.SetOutput(file) // writes log. outputs to designated log file
}
