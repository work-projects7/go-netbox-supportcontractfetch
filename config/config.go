package config

type ConfigData struct {
	ApiToken        string `yaml:"api_token"`
	ApiAddress      string `yaml:"api_address"`
	LogPath         string `yaml:"logpath"`
	StartDate       string `yaml:"start_date"`
	EndDate         string `yaml:"end_date"`
	SupportProvider string `yaml:"support_provider,omitempty"`
}
