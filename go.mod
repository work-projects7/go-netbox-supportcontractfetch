module gitlab.com/work-projects7/go-netbox-supportcontractfetch

go 1.20

require (
	gitlab.com/work-projects7/go-netbox v1.2.3
	gitlab.com/work-projects7/go-netbox-updater v1.2.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/vmware/govmomi v0.29.0 // indirect
	gitlab.com/work-projects7/go-vscanner v1.2.0 // indirect
)
